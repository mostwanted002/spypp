# SPyPP - A simple port prober that is written in Python

## Usage

```
usage: port-prober.py [-h] [--timeout TIMEOUT] ip_addresses ports

positional arguments:
  ip_addresses       IP address(es) in CIDR format
  ports              Comma separated list of ports to probe

options:
  -h, --help         show this help message and exit
  --timeout TIMEOUT  Set timeout for connection response
```
