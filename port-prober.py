#!/usr/bin/env python3

import ipaddress
import socket
import argparse
import sys

default_nmap_ports = ""

def probe_ports(ip_addresses, ports, timeout):
    for address in ip_addresses:
        for port in ports:
            try:
                x = socket.create_connection((address, port), timeout=timeout)
                sys.stdout.buffer.write(f'IP: {address};Open: {port}\n'.encode())
                sys.stdout.buffer.flush()
                x.close()
            except ConnectionRefusedError:
                sys.stdout.buffer.write(f'IP: {address};Close: {port}\n'.encode())
                sys.stdout.buffer.flush()
                pass
            except OSError:
                sys.stdout.buffer.write(f'IP: {address};Unreachable\n'.encode())
                sys.stdout.buffer.flush()
                continue

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("ip_addresses", default=None, help="IP address(es) in CIDR format", type=str)
    parser.add_argument("--ports", default="80", help="Comma separated list of ports to probe", type=str)
    parser.add_argument("--timeout", default=5, help="Set timeout for connection response", type=int)
    args = parser.parse_args()
    if args.ports == "all":
        ports = [int(i) for i in range(0,65536)]
    else:
        ports = [int(port) for port in args.ports.split(',')]
    ip_addresses = [str(ip) for ip in ipaddress.IPv4Network(args.ip_addresses)]
    probe_ports(ip_addresses, ports, args.timeout)
    exit(0)
